#!/usr/bin/env python3
#title           :ilgin_dogan_case_study.py
#description     :Case Study Time Series LSTM.
#author          :ILGIN DOĞAN
#date            :29.07.2020
#version         :1
#usage           :python ilgin_dogan_case_study.py
#notes           : advise run this script python3 environment like anaconda env.
#python_version  :3.6.8
#==============================================================================

## necessary packages for case study
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from keras.preprocessing.sequence import TimeseriesGenerator
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from pandas.tseries.offsets import DateOffset
import warnings
warnings.filterwarnings("ignore")

### Reading data and showing
df = pd.read_csv('CaseData.csv')
print(df.head())

# Month/Time data setting index and showing
df.Month = pd.to_datetime(df.Month)
df = df.set_index('Month')
print(df.head())

# data splitting train and test
train,test = df[:-12],df[-12:]

# train and test data scaling 0 to 1
scaler = MinMaxScaler()
scaler.fit(train)
train = scaler.transform(train)
test = scaler.transform(test)

# parameter for training
n_input = 12
n_features = 1

# LSTM model settings
generator = TimeseriesGenerator(train,train,length=n_input,batch_size=6)
model = Sequential()
model.add(LSTM(200,activation='relu',input_shape=(n_input,n_features)))
model.add(Dropout(0.15))
model.add(Dense(1))
model.compile(optimizer='adam',loss='mse')
model.fit_generator(generator,epochs=180)

# variable definition
pred_list = []
batch = train[-n_input:].reshape((1,n_input,n_features))

# for loop for 12 month prediction value appending
for i in range(n_input):
    pred_list.append(model.predict(batch)[0])
    batch = np.append(batch[:,1:,:],[[pred_list[i]]],axis=1)
# concatenate prediction value
df_predict = pd.DataFrame(scaler.inverse_transform(pred_list),index=df[-n_input:].index,columns=['Predictions'])
df_test = pd.concat([df,df_predict],axis=1)
print(df_test.tail(13))

# results showing on chart
plt.figure(figsize=(12,5))
plt.plot(df_test.index,df_test['#PriceOfWatch$'])
plt.plot(df_test.index,df_test['Predictions'],color='r')
plt.title('TEST PREDICTION',fontsize = 14,color = 'r')
plt.xlabel('Press q for next year prediction',fontsize = 16,color = 'r')
plt.show()

print("Next year prediction")

# Next year prediction training
train = df
scaler.fit(train)
train = scaler.transform(train)
n_input = 12
n_features = 1
generator = TimeseriesGenerator(train,train,length=n_input,batch_size=6)
model.fit_generator(generator,epochs=180)

# variable definition
pred_list = []
batch = train[-n_input:].reshape((1,n_input,n_features))

# for loop for 12 month prediction value appending
for i in range(n_input):
    pred_list.append(model.predict(batch)[0])
    batch = np.append(batch[:,1:,:],[[pred_list[i]]],axis=1)

# Appending and counting months with DateOffset library
add_dates = [df.index[-1] + DateOffset(months=x) for x in range(0,13)]
future_dates = pd.DataFrame(index=add_dates[1:],columns=df.columns)
print(future_dates.tail(12))

# concatenate prediction value
df_predict = pd.DataFrame(scaler.inverse_transform(pred_list),index=future_dates[-n_input:].index,columns=['Predictions'])
df_proj = pd.concat([df,df_predict],axis=1)
print(df_proj.tail(12))

# results showing on chart
plt.figure(figsize=(10,4))
plt.plot(df_proj.index,df_proj['#PriceOfWatch$'])
plt.plot(df_proj.index,df_proj['Predictions'],color='r')
plt.legend(loc='best',fontsize='large')
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.title("NEXT YEAR PREDICTION",fontsize=14,color='r')
plt.show()
